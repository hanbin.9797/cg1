/*
 * TextureCoreFactory.h
 *
 *  Created on: 18.06.2019
 *      Author: dsw-x45-u1
 */

#ifndef FACTORIES_MYTEXTURECOREFACTORY_H_
#define FACTORIES_MYTEXTURECOREFACTORY_H_

#include <../scg3/scg3.h>
using namespace scg;

class MyTextureCoreFactory {
public:
    static Texture2DCoreSP getTexture(const std::string& fileName);

};


#endif /* FACTORIES_MYTEXTURECOREFACTORY_H_ */
