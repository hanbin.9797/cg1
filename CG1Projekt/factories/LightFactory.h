/*
 * LightFactory.h
 *
 *  Created on: 22.05.2019
 *      Author: edh-a0y-u1
 */

#ifndef LIGHTFACTORY_H_
#define LIGHTFACTORY_H_
#include <../scg3/scg3.h>
using namespace scg;

class LightFactory {
public:
    static LightSP defaultLight(NodeSP child);
    static LightSP defaultLight();
    static LightSP flashLight();

};

#endif /* LIGHTFACTORY_H_ */
