/*
 * ObjectFactory.h
 *
 *  Created on: 20.05.2019
 *      Author: edh-a0y-u1
 */

#ifndef SHAPEFACTORY_H_
#define SHAPEFACTORY_H_
#include <../scg3/scg3.h>
using namespace scg;

class ShapeFactory {
public:
	static ShapeSP rectangle(glm::vec2 size);
	static ShapeSP rectangle(glm::vec2 size, MaterialCoreSP material);
	static ShapeSP isoscelesTriangle(GLfloat width, GLfloat height);
	static ShapeSP isoscelesTriangle(GLfloat width, GLfloat height,
			MaterialCoreSP material);
	static ShapeSP closedCylinder(GLfloat radius, GLfloat height, int nSlices,
			int nStacks);
	static ShapeSP closedCylinder(GLfloat radius, GLfloat height, int nSlices,
			int nStacks, MaterialCoreSP material);
	static ShapeSP cube(glm::vec3 size);
	static ShapeSP cube(glm::vec3 size, MaterialCoreSP material);
	static ShapeSP sphere(GLfloat radius, int nSlices, int nStacks);
	static ShapeSP sphere(GLfloat radius, int nSlices, int nStacks, MaterialCoreSP material);
	static ShapeSP conicalFrustum(GLfloat baseRadius, GLfloat topRadius,
		    GLfloat height, int nSlices, int nStacks, bool hasCaps);
	static ShapeSP conicalFrustum(GLfloat baseRadius, GLfloat topRadius,
			    GLfloat height, int nSlices, int nStacks, bool hasCaps, MaterialCoreSP material);
	static ShapeSP skybox(GLfloat size);


};

#endif /* SHAPEFACTORY_H_ */
