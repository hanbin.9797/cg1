/*
 * CompositeFactory.cpp
 *
 *  Created on: 22.05.2019
 *      Author: edh-a0y-u1
 */

#include "MaterialCoreFactory.h"
#include "CompositeFactory.h"

#include "../objects/Bike.h"
#include "../objects/House.h"
#include "../objects/Street.h"
#include "../objects/StreetLightPole.h"
#include "ShapeFactory.h"
#include "ShaderFactory.h"
#include "../util/TransformationUtil.h"



CompositeSP CompositeFactory::street(GLfloat length, GLfloat streetWidth,
        GLfloat shoulderWidth, MaterialCoreSP streetMaterial,
        MaterialCoreSP shoulderMaterial) {
    auto parent = Transformation::create();
    parent->translate(glm::vec3(0.f, -0.5f, 0.f));
    return street(length, streetWidth, shoulderWidth, streetMaterial,
            shoulderMaterial, parent);
}

CompositeSP CompositeFactory::street(GLfloat length, GLfloat streetWidth,
        GLfloat shoulderWidth, MaterialCoreSP streetMaterial,
        MaterialCoreSP shoulderMaterial, CompositeSP parent) {
	auto street = Street::create(length, streetWidth,
	        shoulderWidth, streetMaterial, shoulderMaterial);
    parent->addChild(street);
    return parent;
}

CompositeSP CompositeFactory::houseScene(GLfloat houseWidth, GLfloat houseHight,
		GLfloat houseLength	) {
	auto result = Transformation::create();
	result->translate(glm::vec3(0.f, -0.5f, 0.f));

	auto aHouse = House::create(glm::vec3(houseWidth, houseHight, houseLength));

	// group of house
	result->addChild(TransformationUtil::nodeTranslation(glm::vec3(-25.f, houseHight/2 -1.2, 15.f), aHouse));
	result->addChild(TransformationUtil::nodeTranslation(glm::vec3(-25.f, houseHight/2 -1.2, -15.f), aHouse));
	result->addChild(TransformationUtil::nodeTranslation(glm::vec3(25.f, houseHight/2 -1.2, 15.f), aHouse));
	result->addChild(TransformationUtil::nodeTranslation(glm::vec3(25.f, houseHight/2 -1.2, -15.f), aHouse));

	return result;
}

CompositeSP CompositeFactory::wheel(MaterialCoreSP material) {
//	wheel Group
	auto wheel = Group::create();
	GeometryCoreFactory geometryFactory;

//	roll
	auto rollCore = geometryFactory.createWheel(.05, .5, .2, 10, 20);
	auto roll = Shape::create();
	roll->addCore(material)->addCore(rollCore);
	wheel->addChild(roll);
//	legs
	auto legCore = geometryFactory.createCylinder(.01, .5, 10, 1, 0);
	ShapeSP leg[5];
	TransformationSP legTrans[5];
	for (int i = 0; i < 5; ++i) {
		leg[i] = Shape::create();
		leg[i]->addCore(material)->addCore(legCore);
		legTrans[i] = Transformation::create();
		wheel->addChild(legTrans[i]);
		legTrans[i]->addChild(leg[i]);
		legTrans[i]->rotateRad(i * 2.f * PI / 5, glm::vec3(0.f, 0.f, 1.f));
		legTrans[i]->translate(glm::vec3(-.25f, 0.f, 0.f));
		legTrans[i]->rotateRad(PI / 2, glm::vec3(0.f, 1.f, 0.f));
	}
	return wheel;
}

CompositeSP CompositeFactory::bike(MaterialCoreSP material) {
	return Bike::create(material);
}
