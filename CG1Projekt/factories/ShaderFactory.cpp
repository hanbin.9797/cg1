/*
 * ShaderFactory.cpp
 *
 *  Created on: 24.05.2019
 *      Author: edh-a0y-u1
 */

#include "ShaderFactory.h"
static ShaderCoreFactory shaderFactory(
		"../scg3/shaders;../../scg3/shaders;shaders;../shaders");

ShaderCoreSP ShaderFactory::skybox() {
	return shaderFactory.createShaderFromSourceFiles(
			{ ShaderFile("skybox_vert.glsl", GL_VERTEX_SHADER), ShaderFile(
					"skybox_frag.glsl", GL_FRAGMENT_SHADER) });
}

ShaderCoreSP ShaderFactory::phong() {
    ShaderCoreFactory shaderFactory("../scg3/shaders;../../scg3/shaders");

#ifdef SCG_CPP11_INITIALIZER_LISTS
    return shaderFactory.createShaderFromSourceFiles(
		{
			ShaderFile("phong_vert.glsl", GL_VERTEX_SHADER),
    		ShaderFile("phong_frag.glsl", GL_FRAGMENT_SHADER),
			ShaderFile("blinn_phong_lighting.glsl", GL_FRAGMENT_SHADER),
			ShaderFile("texture_none.glsl", GL_FRAGMENT_SHADER)
		});
#else
    std::vector<ShaderFile> shaderFiles;
    shaderFiles.push_back(ShaderFile("phong_vert.glsl", GL_VERTEX_SHADER));
    shaderFiles.push_back(ShaderFile("phong_frag.glsl", GL_FRAGMENT_SHADER));
    shaderFiles.push_back(ShaderFile("blinn_phong_lighting.glsl", GL_FRAGMENT_SHADER));
    shaderFiles.push_back(ShaderFile("texture_none.glsl", GL_FRAGMENT_SHADER));
    return shaderFactory.createShaderFromSourceFiles(shaderFiles);
#endif
}

ShaderCoreSP ShaderFactory::phongTexture() {
	ShaderCoreFactory shaderFactory("../scg3/shaders;../../scg3/shaders");

#ifdef SCG_CPP11_INITIALIZER_LISTS
	return shaderFactory.createShaderFromSourceFiles(
			{ ShaderFile("phong_vert.glsl", GL_VERTEX_SHADER), ShaderFile(
					"phong_frag.glsl", GL_FRAGMENT_SHADER), ShaderFile(
					"blinn_phong_lighting.glsl", GL_FRAGMENT_SHADER),
					ShaderFile("texture2d_modulate.glsl", GL_FRAGMENT_SHADER) });
#else
	shaderFiles.clear();
	shaderFiles.push_back(ShaderFile("phong_vert.glsl", GL_VERTEX_SHADER));
	shaderFiles.push_back(ShaderFile("phong_frag.glsl", GL_FRAGMENT_SHADER));
	shaderFiles.push_back(ShaderFile("blinn_phong_lighting.glsl", GL_FRAGMENT_SHADER));
	shaderFiles.push_back(ShaderFile("texture2d_modulate.glsl", GL_FRAGMENT_SHADER));
	return shaderFactory.createShaderFromSourceFiles(shaderFiles);
#endif
}

