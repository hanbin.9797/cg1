/*
 * CompositeFactory.h
 *
 *  Created on: 22.05.2019
 *      Author: edh-a0y-u1
 */

#ifndef COMPOSITEFACTORY_H_
#define COMPOSITEFACTORY_H_
#include <../scg3/scg3.h>
using namespace scg;

class CompositeFactory {
public:
    static CompositeSP street(GLfloat length, GLfloat streetWidth,
            GLfloat shoulderWidth, MaterialCoreSP streetMaterial,
            MaterialCoreSP shoulderMaterial);

    static CompositeSP street(GLfloat length, GLfloat streetWidth,
            GLfloat shoulderWidth, MaterialCoreSP streetMaterial,
            MaterialCoreSP shoulderMaterial, CompositeSP parent);

    static CompositeSP houseScene(GLfloat houseWidth, GLfloat houseHight,
    		GLfloat houseLength);

    static CompositeSP brickHouse(GLfloat houseWidth, GLfloat houseHight,
    		GLfloat houseLength);

    static CompositeSP wheel(MaterialCoreSP material);
    static CompositeSP bike(MaterialCoreSP material);
};

#endif /* COMPOSITEFACTORY_H_ */
