/*
 * LightFactory.cpp
 *
 *  Created on: 22.05.2019
 *      Author: edh-a0y-u1
 */

#include "LightFactory.h"

LightSP LightFactory::defaultLight(NodeSP child) {
        LightSP light = defaultLight();
        light->addChild(child);
        return light;
    }

LightSP LightFactory::defaultLight() {
        LightSP light = Light::create();
		light->setDiffuseAndSpecular(glm::vec4(1.f, 1.f, 1.f, 1.f))->setPosition(
				glm::vec4(0.f, 5.f, 0.f, 1.f))->init();
        return light;
    }

LightSP LightFactory::flashLight() {
		LightSP light = Light::create();
		light->setDiffuseAndSpecular(glm::vec4(1.f, 1.f, 1.f, 1.f))->setPosition(
				glm::vec4(0.f, 5.f, 0.f, 1.f))->setSpot(
				glm::vec3(-1.f, -1.f, 0.f), 45, 1)->init();
		return light;
    }
