/*
 * TextureCoreFactory.cpp
 *
 *  Created on: 18.06.2019
 *      Author: dsw-x45-u1
 */


#include "MyTextureCoreFactory.h"

Texture2DCoreSP MyTextureCoreFactory::getTexture(const std::string& fileName){
	TextureCoreFactory textureFactory("../scg3/textures;../../scg3/textures");
	auto texture = textureFactory.create2DTextureFromFile(fileName,
	GL_REPEAT, GL_REPEAT, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR);
	texture->scale2D(glm::vec2(1.f, 1.f));
	return texture;
}

