/*
 * MaterialCoreFactory.h
 *
 *  Created on: 20.05.2019
 *      Author: edh-a0y-u1
 */

#ifndef MATERIALCOREFACTORY_H_
#define MATERIALCOREFACTORY_H_
#include <../scg3/scg3.h>
using namespace scg;

class MaterialCoreFactory {
public:
    static MaterialCoreSP red();
    static MaterialCoreSP green();
    static MaterialCoreSP white();
    static MaterialCoreSP grey();
};

#endif /* MATERIALCOREFACTORY_H_ */
