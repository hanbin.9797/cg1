/*
 * MaterialCoreFactory.cpp
 *
 *  Created on: 20.05.2019
 *      Author: edh-a0y-u1
 */

#include "MaterialCoreFactory.h"

MaterialCoreSP MaterialCoreFactory::red() {
    auto result = MaterialCore::create();
    result->setAmbientAndDiffuse(glm::vec4(1.f, 0.5f, 0.5f, 1.f))
          ->setSpecular(glm::vec4(1.f, 1.f, 1.f, 1.f))
          ->setShininess(20.f)
          ->init();
    return result;
}

MaterialCoreSP MaterialCoreFactory::green() {
    auto result = MaterialCore::create();
    result->setAmbientAndDiffuse(glm::vec4(0.1f, 0.8f, 0.3f, 1.f))
            ->init();
    return result;
}

MaterialCoreSP MaterialCoreFactory::white() {
    auto result = MaterialCore::create();
    result->setAmbientAndDiffuse(glm::vec4(1.f, 1.f, 1.f, 1.f))
            ->setSpecular(glm::vec4(0.5f, 0.5f, 0.5f, 1.f))
            ->setShininess(20.f)
            ->init();
    return result;
}

MaterialCoreSP MaterialCoreFactory::grey() {
    auto result = MaterialCore::create();
    result->setAmbientAndDiffuse(glm::vec4(.5f, .5f, .5f, .5f))
            ->setSpecular(glm::vec4(0.5f, 0.5f, 0.5f, 1.f))
            ->setShininess(20.f)
            ->init();
    return result;
}

