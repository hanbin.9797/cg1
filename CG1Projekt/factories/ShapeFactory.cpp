/*
 * ObjectFactory.cpp
 *
 *  Created on: 20.05.2019
 *      Author: edh-a0y-u1
 */

#include "ShapeFactory.h"
#include "MaterialCoreFactory.h"
#include "../factories/ShaderFactory.h"

#ifndef _BASE_COLOR_
#define _BASE_COLOR_ MaterialCoreFactory::white()
#endif

static GeometryCoreFactory geometryFactory;
static TextureCoreFactory textureFactory(
		"../scg3/textures;../../scg3/textures;textures;../textures");

ShapeSP ShapeFactory::rectangle(glm::vec2 size) {
    return rectangle(size, _BASE_COLOR_);
}

ShapeSP ShapeFactory::rectangle(glm::vec2 size, MaterialCoreSP material) {
    auto result = Shape::create();
    result->addCore(material)->addCore(geometryFactory.createRectangle(size));
    return result;
}

ShapeSP ShapeFactory::isoscelesTriangle(GLfloat width, GLfloat height) {
    return isoscelesTriangle(width, height, _BASE_COLOR_);
}

ShapeSP ShapeFactory::isoscelesTriangle(GLfloat width, GLfloat height, MaterialCoreSP material) {
    auto result = Shape::create();
    result->addCore(material)->addCore(geometryFactory.createIsoscelesTriangle(width, height));
    return result;
}

ShapeSP ShapeFactory::cube(glm::vec3 size) {
    return cube(size, _BASE_COLOR_);
}

ShapeSP ShapeFactory::cube(glm::vec3 size, MaterialCoreSP material) {
    auto result = Shape::create();
    result->addCore(material)->addCore(geometryFactory.createCuboid(size));
    return result;
}

ShapeSP ShapeFactory::closedCylinder(GLfloat radius, GLfloat height,
	    int nSlices, int nStacks) {
    return closedCylinder(radius, height, nSlices, nStacks, _BASE_COLOR_);
}

ShapeSP ShapeFactory::closedCylinder(GLfloat radius, GLfloat height,
	    int nSlices, int nStacks, MaterialCoreSP material) {
    auto result = Shape::create();
    result->addCore(material)->addCore(geometryFactory.createCylinder(radius, height, nSlices, nStacks, true));
    return result;
}

ShapeSP ShapeFactory::sphere(GLfloat radius, int nSlices, int nStacks) {
	return sphere(radius, nSlices, nStacks, _BASE_COLOR_);
}

ShapeSP ShapeFactory::sphere(GLfloat radius, int nSlices, int nStacks, MaterialCoreSP material) {
	auto result = Shape::create();
	result->addCore(material)->addCore(geometryFactory.createSphere(radius, nSlices, nStacks));
	return result;
}

ShapeSP ShapeFactory::conicalFrustum(GLfloat baseRadius, GLfloat topRadius,
		    GLfloat height, int nSlices, int nStacks, bool hasCaps) {
	return conicalFrustum(baseRadius, topRadius, height, nSlices, nStacks,
			hasCaps, _BASE_COLOR_);
}

ShapeSP ShapeFactory::conicalFrustum(GLfloat baseRadius, GLfloat topRadius,
		    GLfloat height, int nSlices, int nStacks, bool hasCaps, MaterialCoreSP material) {
	auto result = Shape::create();
    result->addCore(material)->addCore(geometryFactory.createConicalFrustum
    		(baseRadius, topRadius, height, nSlices, nStacks, hasCaps));
    return result;
}

ShapeSP ShapeFactory::skybox(GLfloat size) {
	auto cubeSkybox = textureFactory.createCubeMapFromFiles( {
			"purplenebula_lf.tga", "purplenebula_rt.tga", "purplenebula_up.tga",
			"purplenebula_dn.tga", "purplenebula_ft.tga", "purplenebula_bk.tga" });
	auto skybox = Shape::create();
	skybox->addCore(ShaderFactory::skybox())->addCore(cubeSkybox)->addCore(geometryFactory.createCube(size));

    return skybox;
}
