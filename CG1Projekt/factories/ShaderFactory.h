/*
 * ShaderFactory.h
 *
 *  Created on: 24.05.2019
 *      Author: edh-a0y-u1
 */

#ifndef FACTORIES_SHADERFACTORY_H_
#define FACTORIES_SHADERFACTORY_H_
#include <../scg3/scg3.h>
using namespace scg;

class ShaderFactory {
public:
	static ShaderCoreSP phong();
	static ShaderCoreSP phongTexture();
	static ShaderCoreSP skybox();
};

#endif /* FACTORIES_SHADERFACTORY_H_ */
