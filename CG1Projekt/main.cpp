#include <iostream>
#include <stdexcept>
#include <vector>
#include <../scg3/scg3.h>

#include "StreetSceneCreator.h"
#include "controllers/BikeKeyboardController.h"

using namespace scg;

void startBikeSimulation();
NodeSP bikeSimulator(CameraSP camera);
void start(ViewerSP& viewer);

/**
 * return 0 for success, 1 for error
 */
int main() {

    int result = 0;

    try {
        startBikeSimulation();
    } catch (const std::exception& exc) {
        std::cerr << std::endl << "Exception: " << exc.what() << std::endl;
        result = 1;
    }
    return result;
}

void startBikeSimulation() {

    // create viewer and renderer
    auto renderer = StandardRenderer::create();
    auto viewer = Viewer::create();
    viewer->init(renderer);
    viewer->createWindow("s c g 3  p r o j e c t ", 1024, 768);

    // create camera
    auto camera = PerspectiveCamera::create(); // must be created after viewer->createWindow
    camera->translate(glm::vec3(0.f, 10.f, 20.f))->dolly(-10.f);
    renderer->setCamera(camera);

    // create scene
    StreetSceneCreator sceneCreator(camera, viewer);
    renderer->setScene(sceneCreator.getScene());

    //camera controller
    auto bikeKeyboardController = BikeKeyboardController::create(camera, sceneCreator.getBike());
    auto keyboardController = KeyboardController::create(camera);
    auto mouseController = MouseController::create(camera);
	viewer->addController(keyboardController)->
			addController(mouseController)->addController(bikeKeyboardController);

    // start animations, enter main loop
    start(viewer);

}

void start(ViewerSP& viewer) {
    viewer->startAnimations()->startMainLoop();
}
