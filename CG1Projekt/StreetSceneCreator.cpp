/*
 * StreetSceneCreator.cpp
 *
 *  Created on: 24.05.2019
 *      Author: edh-a0y-u1
 */

#include "StreetSceneCreator.h"
#include "util/TransformationUtil.h"
#include "factories/LightFactory.h"
#include "factories/CompositeFactory.h"
#include "factories/MaterialCoreFactory.h"
#include "factories/ShaderFactory.h"
#include "factories/ShapeFactory.h"

#include "factories/ShapeFactory.h"
#include "objects/StreetLightPole.h"


StreetSceneCreator::StreetSceneCreator(CameraSP camera, ViewerSP viewer) :
            camera_(camera), viewer_(viewer) {
    	init();
}

void StreetSceneCreator::init() {
	// bike
	bike_ = std::static_pointer_cast<Bike>(CompositeFactory::bike(MaterialCoreFactory::red()));

	// street's ground
	auto street = CompositeFactory::street(800.f, 6.f, 30.f,
			MaterialCoreFactory::grey(), MaterialCoreFactory::green());

	// group of street light poles and their light source
	auto streetLightPole1 = StreetLightPole::create(0.f, -5.f, MaterialCoreFactory::grey(), viewer_);
	auto lightPole1 = streetLightPole1->getLight();
	auto streetLightPole2 = StreetLightPole::create(-70.f, 5.f, MaterialCoreFactory::grey(), viewer_);
	auto lightPole2 = streetLightPole2->getLight();
	auto lightBike = bike_->getLight();

	// houses
	auto house = CompositeFactory::houseScene(10.f, 10.f, 10.f);

	// skybox
	auto skybox = ShapeFactory::skybox(500);

	// shaderPhong
	auto shaderPhong = ShaderFactory::phong();

	// create scene graph
	scene_ = Group::create();
	scene_->addCore(shaderPhong);
	scene_->addChild(camera_)->addChild(skybox)->addChild(lightPole1);
	lightPole1->addChild(lightPole2);
	lightPole2->addChild(lightBike);
	lightBike->addChild(street)->addChild(bike_)->addChild(house)->addChild(
			streetLightPole1)->addChild(streetLightPole2);

}

NodeSP StreetSceneCreator::getScene() {
	return scene_;
}

std::shared_ptr<Bike> StreetSceneCreator::getBike() {
	return bike_;
}

