/*
 * StreetSceneCreator.h
 *
 *  Created on: 24.05.2019
 *      Author: edh-a0y-u1
 */

#ifndef STREETSCENECREATOR_H_
#define STREETSCENECREATOR_H_
#include <../scg3/scg3.h>
using namespace scg;
#include "objects/Bike.h"

class StreetSceneCreator {
private:
    CameraSP camera_;
    GroupSP scene_;
    ViewerSP viewer_;
    std::shared_ptr<Bike> bike_;

public:
    StreetSceneCreator(CameraSP camera, ViewerSP viewer);
    void init();
    NodeSP getScene();
    std::shared_ptr<Bike> getBike();
};

#endif /* STREETSCENECREATOR_H_ */
