/*
 * TransformationUtil.h
 *
 *  Created on: 24.05.2019
 *      Author: edh-a0y-u1
 */

#ifndef UTIL_TRANSFORMATIONUTIL_H_
#define UTIL_TRANSFORMATIONUTIL_H_
#include <../scg3/scg3.h>
using namespace scg;

class TransformationUtil {
public:
    static TransformationSP nodeTranslation(glm::vec3 translation,
            NodeSP node);
};

#endif /* UTIL_TRANSFORMATIONUTIL_H_ */
