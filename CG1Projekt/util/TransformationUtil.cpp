/*
 * TransformationUtil.cpp
 *
 *  Created on: 24.05.2019
 *      Author: edh-a0y-u1
 */

#include "TransformationUtil.h"

TransformationSP TransformationUtil::nodeTranslation(glm::vec3 translationVector,
        NodeSP node) {
    auto result = Transformation::create();
    result->translate(translationVector);
    result->addChild(node);
    return result;
}

