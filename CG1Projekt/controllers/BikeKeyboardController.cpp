/*
 * BikeKeyboardController.cpp
 *
 *  Created on: 28.05.2019
 *      Author: edh-a0y-u1
 */

#include "BikeKeyboardController.h"
#include <iostream>
#include <GLFW/glfw3.h>

BikeKeyboardController::BikeKeyboardController(CameraSP camera,
		std::shared_ptr<Bike> bike) :
		CameraController(camera), bike(bike) {
	moveVelocity_ = 6.0f;
	rotateVelocity_ = 45.0f;
	flightVelocityStep_ = 0.2f;
	std::cout << "Keyboard bike control enabled" << std::endl;
	std::cout << "- o: move bike to left" << std::endl;
	std::cout << "- p: move bike to right" << std::endl;
	std::cout << "- i: turn the bike's light on" << std::endl;
	std::cout << "- u: turn the bike's light off" << std::endl;
	std::cout << std::endl;
}

/**
 * Create shared pointer with given camera transformation.
 */
std::shared_ptr<BikeKeyboardController> BikeKeyboardController::create(
		CameraSP camera,
		std::shared_ptr<Bike> bike) {
	  return std::make_shared<BikeKeyboardController>(camera, bike);
}

/**
 * Check input devices, called by Viewer::startMainLoop().
 *
 * \param viewState view state managed by Viewer, may be modified by controller
 */
void BikeKeyboardController::checkInput(ViewState* viewState) {

	// initialize controller state
	static double lastTime(glfwGetTime());
	GLFWwindow* window = viewState->getWindow();

	// determine time difference
	double currTime = glfwGetTime();
	GLfloat diffTime = static_cast<GLfloat>(currTime - lastTime);
	lastTime = currTime;

	if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS) {
		move(-diffTime);
	}
	if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
		move(diffTime);
	}

	if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS) {
		bike->turnLightOff();
	}
	if (glfwGetKey(window, GLFW_KEY_I) == GLFW_PRESS) {
		bike->turnLightOn();
	}
}

void BikeKeyboardController::move(GLfloat xFactor) {
	bike->move(xFactor * moveVelocity_);
	camera_->translate(glm::vec3(xFactor * moveVelocity_, 0.0f, 0.0f));
}
