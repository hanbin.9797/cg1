/*
 * BikeKeyboardController.h
 *
 *  Created on: 28.05.2019
 *      Author: edh-a0y-u1
 */

#ifndef CONTROLLERS_BIKEKEYBOARDCONTROLLER_H_
#define CONTROLLERS_BIKEKEYBOARDCONTROLLER_H_

#include "../objects/Bike.h"
#include <src/CameraController.h>
#include <../scg3/scg3.h>
using namespace scg;

class BikeKeyboardController: public scg::CameraController {
public:

	BikeKeyboardController(CameraSP camera, std::shared_ptr<Bike> bike);

	/**
	 * Create shared pointer with given camera transformation.
	 */
	static std::shared_ptr<BikeKeyboardController> create(CameraSP camera,
			std::shared_ptr<Bike> bike);

	/**
	 * Check input devices, called by Viewer::startMainLoop().
	 *
	 * \param viewState view state managed by Viewer, may be modified by controller
	 */
	virtual void checkInput(ViewState* viewState);

private:
	std::shared_ptr<Bike> bike;

	void move(GLfloat xFactor);
};

#endif /* CONTROLLERS_BIKEKEYBOARDCONTROLLER_H_ */
