/*
 * StreetLight.h
 *
 *  Created on: 15 Jun 2019
 *      Author: namtran
 */

#ifndef STREETLIGHTPOLE_H_
#define STREETLIGHTPOLE_H_
#include <../scg3/scg3.h>
using namespace scg;

class StreetLightPole: public scg::Group {
public:
	static std::shared_ptr<StreetLightPole> create(GLfloat xTrans,
			GLfloat zTrans, MaterialCoreSP material, ViewerSP viewer);

	void setStreetLightPoleMaterial(MaterialCoreSP streetLightPoleMaterial);

	void setTubeHeight(GLfloat height);

	void setTubeRadius(GLfloat radius);

	void setCubeSize(glm::vec3 cubeSize);

	void setXtrans(GLfloat xTrans);

	void setZtrans(GLfloat zTrans);

	LightSP getLight();

private:
	GLfloat tubeHeight_;
	GLfloat tubeRadius_;
	glm::vec3 cubeSize_;
	GLfloat xTrans_;
	GLfloat zTrans_;
	MaterialCoreSP streetLightPoleMaterial_;
	ViewerSP viewer_;
	LightSP light_;

	TransformationSP tube();
	TransformationSP cube();
	TransformationSP cubeAnimation();
	void init();
};

#endif	/* STREETLIGHTPOLE_H_ */
