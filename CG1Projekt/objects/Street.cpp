/*
 * Street.cpp
 *
 *  Created on: 24.05.2019
 *      Author: edh-a0y-u1
 */

#include "Street.h"

#include "../factories/ShapeFactory.h"
#include "../util/TransformationUtil.h"

std::shared_ptr<Street> Street::create() {
	return std::make_shared<Street>();
}

GroupSP Street::create(GLfloat length, GLfloat streetWidth,
		GLfloat shoulderWidth, MaterialCoreSP streetMaterial,
		MaterialCoreSP shoulderMaterial) {
	auto result = create();
	result->setLength(length);
	result->setStreetWidth(streetWidth);
	result->setStreetMaterial(streetMaterial);
	result->setShoulderWidth(shoulderWidth);
	result->shoulder_ = ShapeFactory::cube(
			glm::vec3(length, 0.05f, shoulderWidth), shoulderMaterial);
	result->init();
	return result;
}

void Street::init() {
	addChild(frontShoulder());
	addChild(backShoulder());
	addChild(road());
	addChild(strips());
}

TransformationSP Street::frontShoulder() {
	return TransformationUtil::nodeTranslation(
			glm::vec3(0.f, 0.f, zMovement()), shoulder_);
}

TransformationSP Street::backShoulder() {
	return TransformationUtil::nodeTranslation(
			glm::vec3(0.f, 0.f, -zMovement()), shoulder_);
}

TransformationSP Street::road() {
	auto road = ShapeFactory::cube(glm::vec3(length_, 0.05f, streetWidth_),
			streetMaterial_);
	return TransformationUtil::nodeTranslation(glm::vec3(), road);
}

GroupSP Street::strips() {
	auto result = Group::create();
	auto shape = ShapeFactory::cube(glm::vec3(2.f, 0.05f, .5f));
	for (int i = 0; i < length_ / 3; i++) {
		result->addChild(strip(shape, stripPosition(i)));
	}
	return result;
}

TransformationSP Street::strip(ShapeSP shape, GLfloat position) {
	return TransformationUtil::nodeTranslation(glm::vec3(position, 0.01f, 0.f),
			shape);
}

GLfloat Street::stripPosition(int i) {
	return 1 + 3 * i + -(length_ / 2);
}

GLfloat Street::zMovement() {
	return (streetWidth_ + shoulderWidth_) / 2;
}

void Street::setLength(GLfloat length) {
	this->length_ = length;
}

void Street::setStreetWidth(GLfloat streetWidth) {
	this->streetWidth_ = streetWidth;
}

void Street::setShoulderWidth(GLfloat shoulderWidth) {
	this->shoulderWidth_ = shoulderWidth;
}

void Street::setStreetMaterial(MaterialCoreSP streetMaterial) {
	this->streetMaterial_ = streetMaterial;
}
