/*
 * Bike.h
 *
 *  Created on: 24.05.2019
 *      Author: dsw-x45-u1
 */

#ifndef OBJECTS_BIKE_H_
#define OBJECTS_BIKE_H_

#include <../scg3/scg3.h>
using namespace scg;

class Bike: public scg::Group {
public:
	static std::shared_ptr<Bike> create(MaterialCoreSP material);
	void move(GLfloat xFactor);
	LightSP getLight();
	void turnLightOn();
	void turnLightOff();

private:
	TransformationSP rootTrans_;
	TransformationSP wheelTrans_;
	TransformationSP rotateWheelTrans_[2];
	LightPositionSP lightPosition_;
	LightSP light_;
	static std::shared_ptr<Bike> create();
	void translateRoot(GLfloat xFactor);
	void rotateWheels(GLfloat degree);

};

#endif /* OBJECTS_BIKE_H_ */
