/*
 * Bike.cpp
 *
 *  Created on: 24.05.2019
 *      Author: dsw-x45-u1
 */

#include "Bike.h"

#include "../factories/CompositeFactory.h"
#include "../factories/LightFactory.h"

std::shared_ptr<Bike> Bike::create() {
	return std::make_shared<Bike>();
}

std::shared_ptr<Bike> Bike::create(MaterialCoreSP material) {
	//	bike Group
	auto bike = create();
	bike->rootTrans_ = Transformation::create();
	bike->wheelTrans_ = Transformation::create();
	bike->rootTrans_->addChild(bike->wheelTrans_);
	bike->addCore(material)->addChild(bike->rootTrans_);

	//	create wheels with animation
	auto wheel = CompositeFactory::wheel(material);
	for (int i = 0; i < 2; i++) {
		bike->rotateWheelTrans_[i] = Transformation::create();
		bike->wheelTrans_->addChild(bike->rotateWheelTrans_[i]);
		bike->rotateWheelTrans_[i]->translate(glm::vec3(-1. + 2 * i, 0.f, 0.f));
		bike->rotateWheelTrans_[i]->addChild(wheel);
	}

	//	frame
	GeometryCoreFactory geometryFactory;
	auto tubeCore = geometryFactory.createCylinder(.03, 1, 10, 1, 1);

	ShapeSP tube[7];
	TransformationSP tubeTrans[7];
	for (int i = 0; i < 7; i++) {
		tube[i] = Shape::create();
		tube[i]->addCore(tubeCore);
		tubeTrans[i] = Transformation::create();
		bike->rootTrans_->addChild(tubeTrans[i]);
		tubeTrans[i]->addChild(tube[i]);
	}

	//	top tube
	tubeTrans[0]->translate(glm::vec3(0.f, 0.866f, 0.f));

	//	down tube
	tubeTrans[1]->rotateRad(2 * PI / 3, glm::vec3(0.f, 0.f, 1.f));
	tubeTrans[1]->translate(glm::vec3(.5f, 0.f, 0.f));

	//	seat tube
	tubeTrans[2]->rotateRad(PI / 3, glm::vec3(0.f, 0.f, 1.f));
	tubeTrans[2]->translate(glm::vec3(.5f, 0.f, 0.f));

	//	chain stay
	for (int i = 0; i < 2; i++) {
		tubeTrans[3 + i]->translate(glm::vec3(.5f, 0.f, -.1 + .2 * i));
	}

	//	seat stay
	for (int i = 0; i < 2; i++) {
		tubeTrans[5 + i]->translate(glm::vec3(1.f, 0.f, -.1 + .2 * i));
		tubeTrans[5 + i]->rotateRad(2 * PI / 3, glm::vec3(0.f, 0.f, 1.f));
		tubeTrans[5 + i]->translate(glm::vec3(.5f, 0.f, 0.f));
	}
	for (int i = 0; i < 7; i++) {
		tubeTrans[i]->rotateRad(PI / 2, glm::vec3(0.f, 1.f, 0.f));
	}

	auto cCore = geometryFactory.createCylinder(.03, .26, 10, 1, 1);
	ShapeSP c[2];
	TransformationSP cTrans[2];
	for (int i = 0; i < 2; i++) {
		c[i] = Shape::create();
		c[i]->addCore(cCore);
		cTrans[i] = Transformation::create();
		bike->rootTrans_->addChild(cTrans[i]);
		cTrans[i]->addChild(c[i]);
		cTrans[i]->translate(glm::vec3(1 - i * 0.5, i * 0.866, 0.f));
	}

	//	head tube
	auto headTubeCore = geometryFactory.createCylinder(.03, 1.5, 10, 1, 1);
	ShapeSP headTube;
	TransformationSP headTubeTrans;
	headTube = Shape::create();
	headTube->addCore(headTubeCore);
	headTubeTrans = Transformation::create();
	bike->rootTrans_->addChild(headTubeTrans);
	headTubeTrans->addChild(headTube);
	headTubeTrans->translate(glm::vec3(-1.f, 0.f, 0.f));
	headTubeTrans->rotateRad(PI / 3, glm::vec3(0.f, 0.f, 1.f));
	headTubeTrans->translate(glm::vec3(.75f, 0.f, 0.f));
	headTubeTrans->rotateRad(PI / 2, glm::vec3(0.f, 1.f, 0.f));

	//	head
	auto headCore = geometryFactory.createCylinder(.03, 1., 10, 1, 1);
	ShapeSP head;
	TransformationSP headTrans;
	head = Shape::create();
	head->addCore(headCore);
	headTrans = Transformation::create();
	bike->rootTrans_->addChild(headTrans);
	headTrans->addChild(head);
	headTrans->translate(glm::vec3(-.25f, 1.299f, 0.f));

	// light on head
	auto lightCore = geometryFactory.createCylinder(.03, .25, 10, 1, 1);
	ShapeSP light;
	TransformationSP lightTrans;
	light = Shape::create();
	light->addCore(lightCore);
	lightTrans = Transformation::create();
	bike->rootTrans_->addChild(lightTrans);
	lightTrans->addChild(light);
	lightTrans->translate(glm::vec3(-.26f, 1.329f, -.2f));
	lightTrans->rotateRad(PI / 2, glm::vec3(0.f, 1.f, 0.f));

	bike->light_ = LightFactory::flashLight();
	bike->light_->setPosition(glm::vec4(-4.f, 1.4f, -.22, 1.f));
	bike->lightPosition_ = LightPosition::create(bike->light_);
	bike->rootTrans_->addChild(bike->lightPosition_);

	//	pedal
	auto pedalCore = geometryFactory.createCylinder(.03, .3, 10, 1, 0);
	ShapeSP pedal;
	TransformationSP pedalTrans;
	pedal = Shape::create();
	pedal->addCore(pedalCore);
	pedalTrans = Transformation::create();
	bike->rootTrans_->addChild(pedalTrans);
	pedalTrans->addChild(pedal);

	return bike;
}

void Bike::move(GLfloat xFactor) {
	translateRoot(xFactor);
	rotateWheels(xFactor * 50);
}

void Bike::turnLightOn() {
	light_->setDiffuseAndSpecular(glm::vec4(0.6f, 0.6f, 0.6f, 1.f))->init();
}

void Bike::turnLightOff() {
	light_->setDiffuseAndSpecular(glm::vec4(0.f, 0.f, 0.f, 1.f))->init();
}

LightSP Bike::getLight() {
	return light_;
}

void Bike::translateRoot(GLfloat xFactor) {
	rootTrans_->translate(glm::vec3(xFactor, 0.f, 0.f));
}

void Bike::rotateWheels(GLfloat degree) {
	rotateWheelTrans_[0]->rotate(degree, glm::vec3(0.f, 0.f, -1.f));
	rotateWheelTrans_[1]->rotate(degree, glm::vec3(0.f, 0.f, -1.f));
}
