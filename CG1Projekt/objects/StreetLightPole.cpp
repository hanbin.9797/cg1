/*
 * StreetLight.c
 *
 *  Created on: 15 Jun 2019
 *      Author: namtran
 */

#include "StreetLightPole.h"

#include "../factories/ShapeFactory.h"
#include "../util/TransformationUtil.h"
#include "../factories/LightFactory.h"
#include "../factories/MaterialCoreFactory.h"

std::shared_ptr<StreetLightPole> StreetLightPole::create(GLfloat xTrans,
		GLfloat zTrans, MaterialCoreSP streetLightPoleMaterial,
		ViewerSP viewer) {
	auto result = std::make_shared<StreetLightPole>();
	result->setTubeHeight(5.f);
	result->setTubeRadius(0.1f);
	result->setCubeSize(glm::vec3(0.5f, 0.5f, 0.5f));
	result->setXtrans(xTrans);
	result->setZtrans(zTrans);
	result->setStreetLightPoleMaterial(streetLightPoleMaterial);
	result->light_ = LightFactory::defaultLight();
	result->viewer_ = viewer;
	result->init();

	return result;
}

LightSP StreetLightPole::getLight() {
	return light_;
}

void StreetLightPole::init() {
	// assemble body and top
	auto parent = Transformation::create();
	parent->addChild(tube());
	parent->addChild(cubeAnimation());

	// adjust position of pole
	light_->setPosition(glm::vec4(xTrans_, tubeHeight_, zTrans_, 1.f));
	parent->translate(glm::vec3(xTrans_, tubeHeight_ / 2 - 0.5, zTrans_));
	parent->rotate(90.f, glm::vec3(-1.f, 0.f, 0.f));
	addChild(parent);
}

TransformationSP StreetLightPole::tube() {
	auto body = ShapeFactory::closedCylinder(tubeRadius_, tubeHeight_, 100, 10,
			streetLightPoleMaterial_);
	return TransformationUtil::nodeTranslation(glm::vec3(0.f, 0.f, 0.f), body);
}

TransformationSP StreetLightPole::cube() {
	auto top = ShapeFactory::cube(cubeSize_);
	auto trans = glm::vec3(0.f, 0.f, tubeHeight_ / 2);
	return TransformationUtil::nodeTranslation(trans, top);
}

TransformationSP StreetLightPole::cubeAnimation() {
	// add animation (rotation)
	auto cubeAnim = TransformAnimation::create();
	float angularVel = 60.f;
	glm::vec3 axis(0.f, 0.f, 1.f);
	cubeAnim->setUpdateFunc([angularVel, axis](TransformAnimation* animation,
			double currTime, double diffTime, double totalTime) {
		animation->rotate(angularVel * static_cast<GLfloat>(diffTime), axis);
	});
	viewer_->addAnimation(cubeAnim);
	cubeAnim->addChild(cube());
	return cubeAnim;
}

void StreetLightPole::setStreetLightPoleMaterial(
		MaterialCoreSP streetLightPoleMaterial) {
	this->streetLightPoleMaterial_ = streetLightPoleMaterial;
}

void StreetLightPole::setTubeHeight(GLfloat tubeHeight) {
	this->tubeHeight_ = tubeHeight;
}

void StreetLightPole::setTubeRadius(GLfloat tubRadius) {
	this->tubeRadius_ = tubRadius;
}

void StreetLightPole::setCubeSize(glm::vec3 cubeSize) {
	this->cubeSize_ = cubeSize;
}

void StreetLightPole::setXtrans(GLfloat xTrans) {
	this->xTrans_ = xTrans;
}

void StreetLightPole::setZtrans(GLfloat zTrans) {
	this->zTrans_ = zTrans;
}
