/*
 * House.cpp
 *
 *  Created on: 24.05.2019
 *      Author: edh-a0y-u1
 */

#include "House.h"

#include "../factories/MyTextureCoreFactory.h"
#include "../factories/ShaderFactory.h"
#include "../factories/ShapeFactory.h"
#include "../util/TransformationUtil.h"


std::shared_ptr<House> House::create() {
	return std::make_shared<House>();
}

GroupSP House::create(glm::vec3 houseSize) {
	auto result = create();
	result->body_ = Group::create();
	result->roof_ = Group::create();
	result->gables_ = Group::create();
	result->init(houseSize);
	return result;
}

void House::init(glm::vec3 houseSize) {
	this->houseSize_ = houseSize;
	initBody("brick_texture.png");
	initRoof("roof_shingles.png");
	initGables("wood_panel.png");
	addChild(body_);
	addChild(roof_);
	addChild(gables_);
	addCore(ShaderFactory::phongTexture());
}

void House::initBody(const std::string& fileName) {
	body_->addCore(MyTextureCoreFactory::getTexture(fileName));
	body_->addChild(
			ShapeFactory::cube(glm::vec3(width(), bodyHeight(), length())));
}

void House::initRoof(const std::string& fileName) {
	auto relief = 1.f;
	auto sides = ShapeFactory::cube(
			glm::vec3(.5f, roofShapeHeight(), length() + relief));
	roof_->addCore(MyTextureCoreFactory::getTexture(fileName));
	roof_->addChild(rightRoof(sides));
	roof_->addChild(leftRoof(sides));
	roof_->addChild(roofTop());
}

TransformationSP House::rightRoof(ShapeSP shape) {
	return adjustedRoofSide(60.f, halfWidth(), shape);
}

TransformationSP House::leftRoof(ShapeSP shape) {
	return adjustedRoofSide(-60.f, -halfWidth(), shape);
}

TransformationSP House::adjustedRoofSide(float degree, GLfloat width,
		ShapeSP shape) {
	auto result = TransformationUtil::nodeTranslation(
			glm::vec3(width, roofHeight(), .0f), shape);
	result->rotate(degree, glm::vec3(0.f, 0.f, 1.f));
	result->translate(glm::vec3(0.f, .5f * roofShapeHeight() - 1.f, 0.f));
	return result;
}

TransformationSP House::roofTop() {
	auto relief = 1.05f;
	auto radius = .31f;
	auto y = .2f + 2 * radius + 0.5 * height();
	auto roofTop = ShapeFactory::closedCylinder(radius, length() + relief, 100,
			10);
	return TransformationUtil::nodeTranslation(glm::vec3(0.f, y, 0.f), roofTop);
}

void House::initGables(const std::string& fileName) {
	auto gable = ShapeFactory::isoscelesTriangle(width(), roofHeight());
	gables_->addCore(MyTextureCoreFactory::getTexture(fileName));
	gables_->addChild(frontGable(gable));
	gables_->addChild(backGable(gable));
}

TransformationSP House::frontGable(ShapeSP shape) {
	return adjustedGable(.5f, shape);
}

TransformationSP House::backGable(ShapeSP shape) {
	return adjustedGable(-.5f, shape);
}

TransformationSP House::adjustedGable(GLfloat zFactor, ShapeSP shape) {
	auto heightAdjustment = 1.5f * roofHeight();
	return TransformationUtil::nodeTranslation(
			glm::vec3(.0f, heightAdjustment, zFactor * length()), shape);
}

/** describes the height of the roof shape at construction */
GLfloat House::roofShapeHeight() {
	return 1.f + sqrt(pow(halfWidth(), 2) + pow(roofHeight(), 2));
}

GLfloat House::halfWidth() {
	return 0.5f * width();
}

GLfloat House::bodyHeight() {
	return height() - roofHeight();
}

GLfloat House::roofHeight() {
	return .33f * height();
}

GLfloat House::height() {
	return houseSize_.y;
}

GLfloat House::width() {
	return houseSize_.x;
}

GLfloat House::length() {
	return houseSize_.z;
}
