/*
 * House.h
 *
 *  Created on: 24.05.2019
 *      Author: edh-a0y-u1
 */

#ifndef OBJECTS_HOUSE_H_
#define OBJECTS_HOUSE_H_
#include <../scg3/scg3.h>
using namespace scg;

class House: public scg::Group {
public:
	/**
	 *\param houseSize (width, height, length)
	 */
	static GroupSP create(glm::vec3 houseSize);


private:
	GroupSP body_;
	GroupSP roof_;
	GroupSP gables_;
	glm::vec3 houseSize_;

	static std::shared_ptr<House> create();
	void init(glm::vec3 houseSize);
	void initBody(const std::string& fileName);
	void initRoof(const std::string& fileName);
	void initGables(const std::string& fileName);
	TransformationSP rightRoof(ShapeSP shape);
	TransformationSP leftRoof(ShapeSP shape);
	TransformationSP adjustedRoofSide(float degree, GLfloat width, ShapeSP shape);
	TransformationSP roofTop();
	TransformationSP frontGable(ShapeSP shape);
	TransformationSP backGable(ShapeSP shape);
	TransformationSP adjustedGable(GLfloat zFactor, ShapeSP shape);
	GLfloat roofShapeHeight();
	GLfloat halfWidth();
	GLfloat bodyHeight();
	GLfloat roofHeight();
	GLfloat height();
	GLfloat width();
	GLfloat length();
//	Texture2DCoreSP getTexture(const std::string& fileName);
};

#endif /* OBJECTS_HOUSE_H_ */
