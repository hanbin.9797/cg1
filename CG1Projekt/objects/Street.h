/*
 * Street.h
 *
 *  Created on: 24.05.2019
 *      Author: edh-a0y-u1
 */

#ifndef STREET_H_
#define STREET_H_

#include <../scg3/scg3.h>
using namespace scg;

class Street: public scg::Group {
public:
	static GroupSP create(GLfloat length, GLfloat streetWidth,
			GLfloat shoulderWidth, MaterialCoreSP streetMaterial,
			MaterialCoreSP shoulderMaterial);

	void setLength(GLfloat length);

	void setStreetWidth(GLfloat streetWidth);

	void setShoulderWidth(GLfloat shoulderWidth);

	void setStreetMaterial(MaterialCoreSP streetMaterial);

private:
	GLfloat length_;
	GLfloat streetWidth_;
	GLfloat shoulderWidth_;
	MaterialCoreSP streetMaterial_;
	ShapeSP shoulder_;

	static std::shared_ptr<Street> create();

	void init();
	GLfloat zMovement();
	TransformationSP frontShoulder();
	TransformationSP road();
	TransformationSP backShoulder();
	GroupSP strips();
	TransformationSP strip(ShapeSP shape, GLfloat position);
	GLfloat stripPosition(int i);
};

#endif /* STREET_H_ */
